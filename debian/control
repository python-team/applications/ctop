Source: ctop
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: ChangZhuo Chen (陳昌倬) <czchen@debian.org>
Section: admin
Priority: optional
Build-Depends: debhelper-compat (= 13),
               asciidoc-base,
               dh-python,
               python3-all,
               python3-minimal,
               python3-setuptools,
               xmlto,
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/python-team/packages/ctop
Vcs-Git: https://salsa.debian.org/python-team/packages/ctop.git
Homepage: https://github.com/yadutaf/ctop

Package: ctop
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
         ${python3:Depends},
Description: Command line / text based Linux Containers monitoring tool
 ctop will help you see what's going on at the container level.
 Basically, containers are a logical group of processes isolated using
 kernel's cgroups and namespaces. Recently, they have been made popular
 by Docker and they are also heavily used under the hood by systemd and a
 load of container tools like lxc, rocket, lmctfy and many others.
 .
 Under the hood, ctop will collect all metrics it can from cgroups in
 realtime and render them to instantly give you an overview of the global
 system health.
 .
 It currently collects metrics related to cpu, memory and block IO usage
 as well as metadata such as owning user (mostly for systemd based
 containers), uptime and attempts to guess the container managing
 technology behind.
